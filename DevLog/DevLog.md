WEEK 01

- Finished the minimum requirements (WASD movement, shooting, simple untextured model made out of primitives)
- Tied the shooting action to spacebar
- Projectiles are on a timer so that the ones out of the player's view will be destroyed
- When moving the player's vessel tilts in the direction it is going to better simulate movement
- I've added only one screenshot this week (not much to be shown so far)

- I've mostly spend the time by learning basics and getting familiar with unity
- In total I'd say I've spent about 4 - 5 hours working on this homework



WEEK 02

- Added an infinite inrush of enemies (maximum of five at a time)
- Gave enemies a simple AI (attempts to get in line with the player and if close to him shoots)
- Added collision detection
	- player with enemy -> remove enemy
	- player projectile with enemy -> remove both
	- player with enemy projectile -> remove enemy projectile
	- enemy with enemy -> Added restriction so that enemies should not move too close to each other
	- enemy with enemy projectile -> nothing
- Added invisible boundaries restricting players movement
- Animated enemies (simple rotation)
- Changed the condition to remove projectiles (now they are removed past certain threshold instead of being on a timer)
- Added a control object that controls what happens in the scene
- Moved the camera further away

- Spent about 6 hours on this assignment



WEEK 03

- Added a health system
- Added a simple UI (Main Menu, Game Over screen, score counter, player Health Bar and enemy Health Bar)
- Added an intro animation
- Added different scenes for the Main Menu, the intro animation and the game
- Added new projectile sprites
- Changed how the enemies behave (now they form two lines to prevent stacking on top of each other)

- Worked for about 10 hours



WEEK 04

- Added three types of weapons - Rapid Fire: shoot projectiles in a straigh tile with a low cooldown (default weapon, use 1/+ to equip)
			       - Spread: shoot three projectile at a time with a medium cooldown (purchasable weapon, use 2/ě to equip)
			       - "Rocket" (not really): shoot a slow hard hitting projectile with a long cooldown (purchasable weapon, use 3/š to equip)
- Added dash in a direction the player is heading (spacebar)
- Changed controls - space now activates dash, to shoot press left mouse button
- Added shop - you can buy weapons and shield (up to 50), to buy items you use your score
	- while in shop you can use + key to gain 1000 score to spend
- Added saves: games automatically saves at every game over screen and after every purchase
- In main menu you can now choose to start a new game (immediately rewrites previous save) or load a saved game
- Added two levels that differ in skybox, number of enemies (level 1 up to five, level 2 up to seven), their health and damage (in level 2 the modifier for both is 150%)
- When starting the game you can choose which level to play

- Spent about 12 hours working



WEEK 05

- Added player and enemy model (both created by me)
- Added particle effects
- Added flame sprite for the player
- Added explosion when an enemy or player dies
- Changed players projectile color to blue

- Worked for about 10 hours



WEEK 06

- Added victory conditions to both levels
	- Gain at least 2000 score in the first level to win
	- Gain at least 3000 score and defeat the boss in the second level to win
- Added boss at the end of the second level (model by me)
	- Weapons:
		- Plasma cannon that targets players position at the time it was fired
		- After reaching this position the missile explodes into six smaller projectiles spread in all directions
		- If the player moves too close to either side of the screen the boss will attempt to chase him out with a high damaging laser
	- Strategy:
		- To reach the boss player must reach at least 3000 score in that level (2000 if the game was not started from the main menu)
		- To defeat the boss the player must destroy two laser generators
		- Laser generator that is currently firing is immune to damage
		- The boss will not shoot its plasma cannon while firing a laser
		- After destroying a generator the boss will no longer fire a laser from the side of that destroyed generator
		- After succesfuly destroying both generators the player must destroy the base ship to win the game
- Added sounds
	- Main menu, in-game and victory screen music
	- Sound for firing weapons
	- Sound for explositonn
	- Sound for clicking menu buttons
	- Sound for gaining score in the shop (via the (+ key))
- Added new sprites for projectiles and laser

- Worked for about 15-16 hours (forgot to add time spent in the original week06 commit)




WEEK 07

- Named the game Space Dash (a very generic name pls don't sue)
- Added PC and Android build
- Modified gameplay for android - the player ship shoots continuously, player only switches weapons
- Added UI for android
	- Added joystick to the left side of the screen
	- Added a dash button
	- Added a switch weapons button
	- Added a plus button in shop to add currency
- Modified boundaries to scale with screen
- Modified UI to scale better with screen

- Worked for about 7 hours
