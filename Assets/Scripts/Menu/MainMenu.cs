﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{	
    public void LevelOne()
	{
		GlobalBehavior.nextScene = 2;
		GlobalBehavior.difficultyModifier = 1f;
		GlobalBehavior.enemyLimit = 5;
		GlobalBehavior.counter = 0;
		SceneManager.LoadScene(1);
	}
	
	public void LevelTwo()
	{
		GlobalBehavior.nextScene = 3;
		GlobalBehavior.difficultyModifier = 1.5f;
		GlobalBehavior.enemyLimit = 7;
		GlobalBehavior.counter = 0;
		SceneManager.LoadScene(1);
	}
	
	public void ExitButton()
	{
		Application.Quit();
	}
	
	public void New()
	{
		GlobalBehavior.playerData = ScriptableObject.CreateInstance<PlayerInfo>();
		
		PlayerPrefs.SetInt("score", 0);
		PlayerPrefs.SetFloat("shield", 0f);
			
		PlayerPrefs.SetInt("rapidFire", 1);
		PlayerPrefs.SetInt("spread", 0);
		PlayerPrefs.SetInt("rockets", 0);
	}
	
	public void Load()
	{
		GlobalBehavior.playerData = ScriptableObject.CreateInstance<PlayerInfo>();
		
		GlobalBehavior.playerData.score = PlayerPrefs.GetInt("score");
		GlobalBehavior.playerData.shield = PlayerPrefs.GetFloat("shield");
		
		GlobalBehavior.playerData.rapidFire = PlayerPrefs.GetInt("rapidFire") != 0;
		GlobalBehavior.playerData.spread = PlayerPrefs.GetInt("spread") != 0;
		GlobalBehavior.playerData.rockets = PlayerPrefs.GetInt("rockets") != 0;
	}
	
	public void PlaySound()
	{
		GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
	}
}
