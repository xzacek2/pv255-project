﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFire : MonoBehaviour
{
    public float speed;
	public float damage;
	public GameObject explosion;
	
    void Update()
    {
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		if (transform.position.x > 13f) {
			Destroy(gameObject);
			return;
		}
		
		float ySin = Mathf.Sin(((transform.eulerAngles.y + 180f) / 180f) * Mathf.PI);
		float yCos = Mathf.Cos(((transform.eulerAngles.y + 180f) / 180f) * Mathf.PI);
        transform.position += new Vector3(speed * Time.deltaTime * ySin, 0f, speed * Time.deltaTime * yCos);
    }
	
	void OnTriggerEnter(Collider col)
	{
		switch (col.gameObject.tag) {
			case("enemyship"):
				EnemyShip s1 = col.gameObject.GetComponent<EnemyShip>();
				s1.curHealth -= damage;
				if (s1.curHealth <= 0f) {
					GlobalBehavior.playerData.score += (int) (100 * GlobalBehavior.difficultyModifier);
					GlobalBehavior.levelScore += (int) (100 * GlobalBehavior.difficultyModifier);
					GlobalBehavior.counter--;
					Instantiate(explosion, col.transform.position - new Vector3(1f, 0f, 0f), Quaternion.Euler(new Vector3(90f, 0f, 0f)));
					Destroy(col.gameObject);
				}
				Destroy(gameObject);
				break;
			case("bossgenerator"):
				Generator s2 = col.gameObject.GetComponent<Generator>();
				if (!s2.isImmune)
					s2.curHealth -= damage;
				if (s2.curHealth <= 0f) {
					GlobalBehavior.playerData.score += 500;
					GlobalBehavior.levelScore += 500;
					GameObject expl = Instantiate(explosion, col.transform.position - new Vector3(2f, 0f, 0f), Quaternion.Euler(new Vector3(90f, 0f, 0f)));
					expl.transform.localScale = new Vector3(2f, 2f, 2f);
					col.transform.parent.Find("Base").gameObject.GetComponent<Base>().dmgMultiplier += 0.5f;
					col.gameObject.SetActive(false);
				}
				Destroy(gameObject);
				break;
			case("bossbase"):
				Base s3 = col.gameObject.GetComponent<Base>();
				s3.curHealth -= damage * (float) ((int) s3.dmgMultiplier);
				if (s3.curHealth <= 0f) {
					GlobalBehavior.bossDefeated = true;
					GameObject hp = GameObject.FindWithTag("bosshp");
					hp.SetActive(false);
					GlobalBehavior.playerData.score += 1000;
					GlobalBehavior.levelScore += 1000;
					GameObject expl = Instantiate(explosion, col.transform.position - new Vector3(4f, 0f, 0f), Quaternion.Euler(new Vector3(90f, 0f, 0f)));
					expl.transform.localScale = new Vector3(4f, 4f, 4f);
					Destroy(col.transform.parent.parent.gameObject);
				}
				Destroy(gameObject);
				break;
		}
	}
}
