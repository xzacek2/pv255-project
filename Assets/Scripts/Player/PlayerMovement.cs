﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public Rigidbody player;
	float dashCooldown = 0f;

	#if UNITY_STANDALONE
	public float maxSpeed;
	public float acceleration;
	
	#elif UNITY_ANDROID
	Vector3 vel;	
	public Joystick joystick;
	#endif
	
	#if UNITY_STANDALONE
    void Update()
    {
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		int coef = (int) (2.0f - (player.velocity.magnitude / maxSpeed));
		
        if (Input.GetKey("w") || Input.GetKey("s")) {
			if (Input.GetKey("w") && !Input.GetKey("s")) {
				player.AddForce((acceleration * Time.deltaTime) * coef, 0, 0);
			} else if (!Input.GetKey("w") && Input.GetKey("s")) {
				player.AddForce((-acceleration * Time.deltaTime) * coef, 0, 0);
			}
		}
		
		if (Input.GetKey("a") || Input.GetKey("d")) {
			if (Input.GetKey("a") && !Input.GetKey("d")) {
				player.AddForce(0, 0, (acceleration * Time.deltaTime) * coef);
			} else if (!Input.GetKey("a") && Input.GetKey("d")) {
				player.AddForce(0, 0, (-acceleration * Time.deltaTime) * coef);
			}
		}
		
		if (dashCooldown > 0)
			dashCooldown -= Time.deltaTime;
		else if (Input.GetKey(KeyCode.Space)) {
			dashCooldown = 1f;
			player.AddForce(player.velocity.x * 500f, 0f, player.velocity.z * 500f);
		}
		
		Vector3 rotationVector = new Vector3(-90f, 0f, 180f);
        transform.rotation = Quaternion.Euler(rotationVector);
		transform.Rotate(Mathf.Clamp(-player.velocity.z * 2, -20f, 20f), Mathf.Clamp(-player.velocity.x * 2, -20f, 20f), 0f);
    }
	
	#elif UNITY_ANDROID
	void Update()
    {
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		vel = new Vector3(joystick.Direction.y, 0f, -joystick.Direction.x);
			
		if (vel.magnitude > 0.4f)
			transform.position += vel.normalized * (8f * Time.deltaTime);
		
		if (dashCooldown > 0f)
			dashCooldown -= Time.deltaTime;
		
		Vector3 rotationVector = new Vector3(-90f, 0f, 180f);
        transform.rotation = Quaternion.Euler(rotationVector);
		transform.Rotate(Mathf.Clamp(joystick.Direction.x * 16f, -20f, 20f), Mathf.Clamp(-joystick.Direction.y * 16f, -20f, 20f), 0f);
    }

	public void Dash()
	{
		
		if (dashCooldown <= 0f) {
			player.AddForce(vel.x * 2000f, 0f, vel.z * 2000f);
			dashCooldown = 1f;
		}
    }
	#endif
}
