﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum WeaponType { RAPIDFIRE, SPREAD, ROCKET };

public class PlayerCombat : MonoBehaviour
{
	public WeaponType weapon;
	public GameObject rapidFireProjectile;
	public GameObject spreadProjectile;
	public GameObject rocketProjectile;
	
	float fireCooldown = 0f;
	float switchCooldown = 0f;
	
	public Slider hp;
	public Slider shield;
	
	public Slider hp_android;
	public Slider shield_android;
	
	public float maxHealth = 100f;
	public float curHealth = 100f;
	
	public float maxShield = 50f;
	public float curShield = 0f;
	
	void Start()
	{
		curShield = GlobalBehavior.playerData.shield;
	}

	#if UNITY_STANDALONE
    void Update()
    {
		hp.value = curHealth / maxHealth;
		shield.value = curShield / maxShield;
		if (curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
	
		if (fireCooldown > 0)
			fireCooldown -= Time.deltaTime;
		else if (Input.GetMouseButton(0)) {
			GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
			switch(weapon) {
				case WeaponType.RAPIDFIRE:
					fireCooldown = 0.2f;
					Instantiate(rapidFireProjectile, transform.position + new Vector3(1f, -0.6f, 0f), rapidFireProjectile.transform.rotation);
					break;
				case WeaponType.SPREAD:
					fireCooldown = 0.4f;
					Instantiate(spreadProjectile, transform.position + new Vector3(1f, -0.6f, 0f), spreadProjectile.transform.rotation);
					Instantiate(spreadProjectile, transform.position + new Vector3(1f, -0.6f, -0.2f), new Quaternion(0.6f, -0.4f, 0.4f, 0.6f));
					Instantiate(spreadProjectile, transform.position + new Vector3(1f, -0.6f, 0.2f), new Quaternion(0.4f, -0.6f, 0.6f, 0.4f));
					break;
				case WeaponType.ROCKET:
					fireCooldown = 1f;
					Instantiate(rocketProjectile, transform.position + new Vector3(1.5f, -0.6f, 0f), rocketProjectile.transform.rotation);
					break;
			}
		}
		
		if (switchCooldown > 0)
			switchCooldown -= Time.deltaTime;
		else if (Input.GetKeyDown(KeyCode.Alpha1)) {
			switchCooldown = 1f;
			weapon = WeaponType.RAPIDFIRE;
		} else if (Input.GetKeyDown(KeyCode.Alpha2) && GlobalBehavior.playerData.spread) {
			switchCooldown = 1f;
			weapon = WeaponType.SPREAD;
		} else if (Input.GetKeyDown(KeyCode.Alpha3) && GlobalBehavior.playerData.rockets) {
			switchCooldown = 1f;
			weapon = WeaponType.ROCKET;
		}
    }
	
	#elif UNITY_ANDROID
    void Update()
    {
		hp_android.value = curHealth / maxHealth;
		shield_android.value = curShield / maxShield;
		if (curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		if (fireCooldown > 0)
			fireCooldown -= Time.deltaTime;
		else {
			GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
			switch(weapon) {
				case WeaponType.RAPIDFIRE:
					fireCooldown = 0.2f;
					Instantiate(rapidFireProjectile, transform.position + new Vector3(1f, -0.6f, 0f), rapidFireProjectile.transform.rotation);
					break;
				case WeaponType.SPREAD:
					fireCooldown = 0.4f;
					Instantiate(spreadProjectile, transform.position + new Vector3(1f, -0.6f, 0f), spreadProjectile.transform.rotation);
					Instantiate(spreadProjectile, transform.position + new Vector3(1f, -0.6f, -0.2f), new Quaternion(0.6f, -0.4f, 0.4f, 0.6f));
					Instantiate(spreadProjectile, transform.position + new Vector3(1f, -0.6f, 0.2f), new Quaternion(0.4f, -0.6f, 0.6f, 0.4f));
					break;
				case WeaponType.ROCKET:
					fireCooldown = 1f;
					Instantiate(rocketProjectile, transform.position + new Vector3(1.5f, -0.6f, 0f), rocketProjectile.transform.rotation);
					break;
			}
		}
		
		if (switchCooldown > 0)
			switchCooldown -= Time.deltaTime;
    }

	public void SwitchWeapon()
	{
		if (switchCooldown <= 0f) {
			if (weapon == WeaponType.RAPIDFIRE && GlobalBehavior.playerData.spread) {
				switchCooldown = 1f;
				weapon = WeaponType.SPREAD;
			} else if (weapon == WeaponType.RAPIDFIRE && GlobalBehavior.playerData.rockets) {
				switchCooldown = 1f;
				weapon = WeaponType.ROCKET;
			} else if (weapon == WeaponType.SPREAD && GlobalBehavior.playerData.rockets) {
				switchCooldown = 1f;
				weapon = WeaponType.ROCKET;
			} else if (weapon == WeaponType.SPREAD) {
				switchCooldown = 1f;
				weapon = WeaponType.RAPIDFIRE;
			} else if (weapon == WeaponType.ROCKET) {
				switchCooldown = 1f;
				weapon = WeaponType.RAPIDFIRE;
			}
		}
	}
	#endif
}
