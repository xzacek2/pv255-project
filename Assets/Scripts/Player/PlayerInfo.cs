﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Info", menuName = "Player Info")]
public class PlayerInfo : ScriptableObject
{
	public GameObject player;
	
	public bool rapidFire = true;
	public bool spread = false;
	public bool rockets = false;
	public float shield = 0f;
	
	public int score = 0;
}
