﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GlobalBehavior : MonoBehaviour
{
	public static PlayerInfo playerData;
	
    public GameObject enemyShip;
	public GameObject boss;
	public GameObject ui;
	public GameObject uiAndroid;
	public GameObject explosion;
	
	public Text score_text;
	public Text score_text_android;
	public GameObject gameOver;
	public GameObject bossHP;
	public GameObject bossHPAndroid;
	public AudioClip victoryAudio;
	bool skipGameOver = false;
	
	public static int nextScene = 2;
	public static float difficultyModifier = 1f;
	public static int enemyLimit = 5;
	public static int counter = 0;
	public static int levelScore;
	public static bool bossDefeated = false;
	bool bossIsSpawned;
	
	public float timer = 0f;
	
	void Awake()
	{
		if (playerData == null)
			GlobalBehavior.playerData = ScriptableObject.CreateInstance<PlayerInfo>();
		playerData.player = GameObject.FindWithTag("playership");
		levelScore = 0;
		nextScene = SceneManager.GetActiveScene().buildIndex;
		bossIsSpawned = false;
		bossDefeated = false;
	}

    void LateUpdate()
    {
		if (bossDefeated) {
			if (skipGameOver)
				return;
			skipGameOver = true;
			
			ui.SetActive(false);
			uiAndroid.SetActive(false);
			GameObject victory = Instantiate(gameOver, new Vector3(0f, 0f, 0f), new Quaternion(0f, 0f, 0f, 0f));
			victory.transform.Find("GameOver").Find("Game Over").gameObject.SetActive(false);
			victory.transform.Find("GameOver").Find("You Win").gameObject.SetActive(true);
			
			Destroy(GameObject.FindWithTag("audioplayer"));
			AudioSource audio = gameObject.AddComponent<AudioSource>();
			audio.PlayOneShot(victoryAudio);
			
			playerData.player.GetComponent<PlayerCombat>().enabled = false;
			playerData.player.GetComponent<PlayerMovement>().enabled = false;
			
			PlayerPrefs.SetInt("score", playerData.score);
			PlayerPrefs.SetFloat("shield", playerData.shield);
			
			if (playerData.rapidFire) PlayerPrefs.SetInt("rapidFire", 1);
			else PlayerPrefs.SetInt("rapidFire", 0);
			
			if (playerData.spread) PlayerPrefs.SetInt("spread", 1);
			else PlayerPrefs.SetInt("spread", 0);
			
			if (playerData.rockets) PlayerPrefs.SetInt("rockets", 1);
			else PlayerPrefs.SetInt("rockets", 0);
			
			return;
		}
		
		if (playerData.player.GetComponent<PlayerCombat>().curHealth <= 0) {
			if (skipGameOver)
				return;
			skipGameOver = true;
			
			Destroy(GameObject.FindWithTag("audioplayer"));
			GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 0.5f);
			GameObject expl = Instantiate(explosion, playerData.player.transform.position + new Vector3(-1f, 0f, 0f), Quaternion.Euler(new Vector3(90f, 0f, 0f)));
			expl.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
			playerData.player.SetActive(false);
			
			ui.SetActive(false);
			uiAndroid.SetActive(false);
			Instantiate(gameOver, new Vector3(0f, 0f, 0f), new Quaternion(0f, 0f, 0f, 0f));
			
			PlayerPrefs.SetInt("score", playerData.score);
			PlayerPrefs.SetFloat("shield", playerData.shield);
			
			if (playerData.rapidFire) PlayerPrefs.SetInt("rapidFire", 1);
			else PlayerPrefs.SetInt("rapidFire", 0);
			
			if (playerData.spread) PlayerPrefs.SetInt("spread", 1);
			else PlayerPrefs.SetInt("spread", 0);
			
			if (playerData.rockets) PlayerPrefs.SetInt("rockets", 1);
			else PlayerPrefs.SetInt("rockets", 0);
			
			return;
		}
		
		if (timer > 0f)
			timer -= Time.deltaTime;
		
        if (counter < enemyLimit && timer <= 0f && levelScore < 2000 * difficultyModifier) {
			int closeCounter = 0;
			int middleCounter = 0;
			GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemyship");
			foreach (GameObject go in enemies) {
				if (go.GetComponent<EnemyShip>().xPos == 4f)
					closeCounter++;
				if (go.GetComponent<EnemyShip>().xPos == 7f)
					middleCounter++;
			}
			
			timer = 1f;
			float z1 = Camera.main.ScreenToWorldPoint(new Vector3(0f, Screen.height / 2f, 20f)).z - 3f;
			float z2 = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height / 2f, 20f)).z + 3f;
			GameObject enemy = Instantiate(enemyShip, new Vector3(Random.Range(15f, 33f), 0f, Random.Range(z1, z2)), Quaternion.Euler(new Vector3(-90f, 0f, 0f)));
			counter++;

			if (closeCounter >= 3 && middleCounter < 3)
				enemy.GetComponent<EnemyShip>().xPos = 7f;
			else if (closeCounter >= 3 && middleCounter >= 3)
				enemy.GetComponent<EnemyShip>().xPos = 10f;
		} else if (!bossIsSpawned && counter <= 0 && levelScore >= 2000 * difficultyModifier && nextScene == 3) {
			bossIsSpawned = true;
			if (ui.activeSelf)
				bossHP.SetActive(true);
			if (uiAndroid.activeSelf)
				bossHPAndroid.SetActive(true);
			GameObject bossGO = Instantiate(boss, new Vector3(0f, 0f, 0f), Quaternion.Euler(0f, 0f, 0f));
			bossGO.transform.Find("Center").position = new Vector3(20f, 0f, 0f);
		} else if (counter <= 0 && levelScore >= 2000 * difficultyModifier && nextScene == 2) {
			bossDefeated = true;
		}
		
		score_text.text = playerData.score.ToString();
		score_text_android.text = playerData.score.ToString();
    }
}
