﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Shop : MonoBehaviour
{
	public Text score_text;
	public AudioClip getScoreAudio;
	
	public void Start()
	{
		if (GlobalBehavior.playerData.rockets) {
			GameObject rockets = transform.Find("Rocket").gameObject;
			rockets.SetActive(false);
		} else {
			GameObject rockets = transform.Find("Rocket").gameObject;
			rockets.SetActive(true);
		}
		
		if (GlobalBehavior.playerData.spread) {
			GameObject spread = transform.Find("Spread").gameObject;
			spread.SetActive(false);
		} else {
			GameObject spread = transform.Find("Spread").gameObject;
			spread.SetActive(true);
		}
		
		if (GlobalBehavior.playerData.shield >= 50f) {
			GameObject shield = transform.Find("Shield").gameObject;
			shield.SetActive(false);
		} else {
			GameObject shield = transform.Find("Shield").gameObject;
			shield.SetActive(true);
		}
	}
	
	public void Update()
	{
		if (GlobalBehavior.playerData.rockets) {
			GameObject rockets = transform.Find("Rocket").gameObject;
			rockets.SetActive(false);
		} else {
			GameObject rockets = transform.Find("Rocket").gameObject;
			rockets.SetActive(true);
		}
		
		if (GlobalBehavior.playerData.spread) {
			GameObject spread = transform.Find("Spread").gameObject;
			spread.SetActive(false);
		} else {
			GameObject spread = transform.Find("Spread").gameObject;
			spread.SetActive(true);
		}
		
		if (GlobalBehavior.playerData.shield >= 50f) {
			GameObject shield = transform.Find("Shield").gameObject;
			shield.SetActive(false);
		} else {
			GameObject shield = transform.Find("Shield").gameObject;
			shield.SetActive(true);
		}
		
		if (Input.GetKeyDown(KeyCode.KeypadPlus)) {
			GlobalBehavior.playerData.score += 1000;
			AudioSource audio = gameObject.AddComponent<AudioSource>();
			audio.PlayOneShot(getScoreAudio);
			PlayerPrefs.SetInt("score", GlobalBehavior.playerData.score);
		}
		
		score_text.text = GlobalBehavior.playerData.score.ToString();
	}
	
	public void AddScore()
	{
		GlobalBehavior.playerData.score += 1000;
		AudioSource audio = gameObject.AddComponent<AudioSource>();
		audio.PlayOneShot(getScoreAudio);
		PlayerPrefs.SetInt("score", GlobalBehavior.playerData.score);
	}
}
