﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour
{
    public ShopItem item;
	
	public void Start()
	{
		transform.Find("Text").gameObject.GetComponent<Text>().text = item.shopItemName + '\n' + item.cost.ToString();
	}
    
	public void Buy()
	{
		if (GlobalBehavior.playerData.score >= item.cost && !GlobalBehavior.playerData.rockets) {
			GlobalBehavior.playerData.rockets = true;
			GlobalBehavior.playerData.score -= item.cost;
			PlayerPrefs.SetInt("rockets", 1);
			PlayerPrefs.SetInt("score", GlobalBehavior.playerData.score);
			gameObject.SetActive(false);
		}
	}
}
