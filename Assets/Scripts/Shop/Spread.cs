﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Spread : MonoBehaviour
{
    public ShopItem item;
	
	public void Start()
	{
		transform.Find("Text").gameObject.GetComponent<Text>().text = item.shopItemName + '\n' + item.cost.ToString();
	}
    
	public void Buy()
	{
		if (GlobalBehavior.playerData.score >= item.cost && !GlobalBehavior.playerData.spread) {
			GlobalBehavior.playerData.spread = true;
			GlobalBehavior.playerData.score -= item.cost;
			PlayerPrefs.SetInt("spread", 1);
			PlayerPrefs.SetInt("score", GlobalBehavior.playerData.score);
			gameObject.SetActive(false);
		}
	}
}
