﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Shield : MonoBehaviour
{
    public ShopItem item;
	
	public void Start()
	{
		transform.Find("Text").gameObject.GetComponent<Text>().text = item.shopItemName + '\n' + item.cost.ToString();
	}
    
	public void Buy()
	{
		if (GlobalBehavior.playerData.score >= item.cost && GlobalBehavior.playerData.shield < 50f) {
			GlobalBehavior.playerData.shield += 10f;
			GlobalBehavior.playerData.score -= item.cost;
			PlayerPrefs.SetFloat("shield", GlobalBehavior.playerData.shield);
			PlayerPrefs.SetInt("score", GlobalBehavior.playerData.score);
		}
		
		if (GlobalBehavior.playerData.shield >= 50f)
			gameObject.SetActive(false);
	}
}
