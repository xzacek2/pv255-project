﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shop Item", menuName = "Shop Item")]
public class ShopItem : ScriptableObject
{
	public string shopItemName;
	public int cost;
}
