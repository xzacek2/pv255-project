﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class IntroSceneAnimaion : MonoBehaviour
{
	public Material skyboxLevelOne;
	public Material skyboxLevelTwo;
	
	public void Start()
	{
		if (GlobalBehavior.nextScene == 2)
			RenderSettings.skybox = skyboxLevelOne;
		else
			RenderSettings.skybox = skyboxLevelTwo;
	}
	
	public void Play()
	{
		SceneManager.LoadScene(GlobalBehavior.nextScene);
	}
}
