﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundariesScript : MonoBehaviour
{
	Camera cam;
	
    void Start()
    {
		cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
		
		float w = Screen.currentResolution.width;
		float h = Screen.currentResolution.height;
		
        transform.position = cam.ScreenToWorldPoint(new Vector3(w / 2f, 0f, 20f));
		transform.Find("Bottom").position = cam.ScreenToWorldPoint(new Vector3(w / 2f, h, 20f));
		transform.Find("Left").position = cam.ScreenToWorldPoint(new Vector3(0f, h / 2f, 20f));
		transform.Find("Right").position = cam.ScreenToWorldPoint(new Vector3(w, h / 2f, 20f));
    }
}
