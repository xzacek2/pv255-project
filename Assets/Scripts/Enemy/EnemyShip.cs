﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyShip : MonoBehaviour
{
	public Rigidbody enemyShip;
	public GameObject enemyProjectile;
	public GameObject explosion;
	public GameObject canvas;
	public GameObject hp;
	Camera cam;
	
	public float maxSpeed;
	float timer = 0f;
	public float xPos = 4f;
	
	public float maxHealth = 100f;
	public float curHealth = 100f;
	
	void Start()
	{
		cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
		maxHealth *= GlobalBehavior.difficultyModifier;
		curHealth *= GlobalBehavior.difficultyModifier;
	}
	
	void LateUpdate()
	{
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated) {
			return;
		}
		
		int coef = (int) (2.0f - (enemyShip.velocity.magnitude / maxSpeed));
		
		if (timer > 0f)
			timer -= Time.deltaTime;
		
		if (transform.position.x > xPos + 0.001f) {
			enemyShip.AddForce(-1000f * Time.deltaTime * coef, 0f, 0f);
			transform.Rotate(0f, 0f, 100f * Time.deltaTime);
		} else {
			if (GlobalBehavior.playerData.player.transform.position.z < enemyShip.position.z - 0.25f) {
				enemyShip.AddForce(0f, 0f, -1000f * Time.deltaTime * coef);
				transform.Rotate(0f, 0f, 100f * Time.deltaTime);
			} else if (GlobalBehavior.playerData.player.transform.position.z > enemyShip.position.z + 0.25f) {
				enemyShip.AddForce(0f, 0f, 1000f * Time.deltaTime * coef);
				transform.Rotate(0f, 0f, -100f * Time.deltaTime);
			}
		}
		
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemyship");
		foreach (GameObject go in enemies) {
			if (Vector3.Distance(go.transform.position, transform.position) < 3f) {
				Vector3 towards = go.transform.position;
				towards += (transform.position - go.transform.position) * 2f;
				if (transform.position.x <= xPos) {
					towards -= new Vector3(towards.x, 0f, 0f);
					towards += new Vector3(xPos, 0f, 0f);
				}
				float distance = 3f - Vector3.Distance(go.transform.position, transform.position);
				transform.position = Vector3.MoveTowards(transform.position, towards, distance);
			}
		}
		
		if (transform.position.x < 12f && Mathf.Abs(GlobalBehavior.playerData.player.transform.position.z - transform.position.z) < 1.5f && timer <= 0f) {
			GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
			Instantiate(enemyProjectile, transform.position - new Vector3(1f, 0f, 0f), enemyProjectile.transform.rotation);
			timer = 1f;
		}
		
		hp.GetComponent<Slider>().value = curHealth / maxHealth;
		canvas.transform.position = transform.position + new Vector3(1.2f, 0f, 0f);
		canvas.transform.LookAt(canvas.transform.position + cam.transform.forward);
		canvas.transform.Rotate(0f, 0f, 90f);
	}

	void OnCollisionEnter(Collision col)
	{
		switch(col.gameObject.tag) {
			case "playership":
				PlayerCombat script = GlobalBehavior.playerData.player.GetComponent<PlayerCombat>();
				float remainingDamage = 25f * GlobalBehavior.difficultyModifier - script.curShield;
				script.curShield = Mathf.Clamp(script.curShield - 25f * GlobalBehavior.difficultyModifier, 0f, 50f);
				if (remainingDamage > 0f)
					script.curHealth -= remainingDamage;
				Instantiate(explosion, transform.position - new Vector3(1f, 0f, 0f), Quaternion.Euler(new Vector3(90f, 0f, 0f)));
				Destroy(gameObject);
				GlobalBehavior.counter--;
				break;
		}
	}
}
