﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFire : MonoBehaviour
{
    public float speed;
	public float damage;

    void Update()
    {
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		if (transform.position.x < -13f || transform.position.x > 13f ||
			transform.position.z < -21f || transform.position.z > 21f) {
				Destroy(gameObject);
				return;
		}
		
		float ySin = Mathf.Sin(((transform.eulerAngles.y + 180f) / 180f) * Mathf.PI);
		float yCos = Mathf.Cos(((transform.eulerAngles.y + 180f) / 180f) * Mathf.PI);
        transform.position += new Vector3(speed * Time.deltaTime * ySin, 0f, speed * Time.deltaTime * yCos);
    }
	
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "playership") {
			PlayerCombat script = col.gameObject.GetComponent<PlayerCombat>();
			float remainingDamage = damage * GlobalBehavior.difficultyModifier - script.curShield;
			script.curShield = Mathf.Clamp(script.curShield - damage * GlobalBehavior.difficultyModifier, 0f, 50f);
			if (remainingDamage > 0f)
				script.curHealth -= remainingDamage;
			Destroy(gameObject);
		}
	}
}
