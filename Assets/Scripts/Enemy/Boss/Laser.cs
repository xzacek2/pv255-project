﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour {

    LineRenderer lineRenderer;
	
	int layer;
	float offset = 0f;
	float timer = 0f;
	float audioTimer = 0f;

	void Start () {
		GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
		layer = LayerMask.GetMask("Player");
        lineRenderer = GetComponent<LineRenderer>();
		GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(0.25f, 1f));
	}
	
	void LateUpdate () {
		audioTimer += Time.deltaTime;
		if (audioTimer > 0.2f) {
			audioTimer = 0f;
			GetComponent<AudioSource>().Stop();
			GetComponent<AudioSource>().PlayOneShot(GetComponent<AudioSource>().clip, 1f);
		}
		
		RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 100f, layer)) {
			if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curShield > 0f)
				GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curShield -= Time.deltaTime * 500f;
			else
				GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth -= Time.deltaTime * 500f;
			lineRenderer.SetPosition(1, hit.point);
        } else
			lineRenderer.SetPosition(1, transform.forward * 5000);
		
		if (timer > 0f) {
			timer -= Time.deltaTime;
		} else {
			timer = 1f / 12f;
			switch(offset) {
				case(0f):
					offset = 0.25f;
					break;
				case(0.25f):
					offset = 0.5f;
					break;
				case(0.5f):
					offset = 0.75f;
					break;
				case(0.75f):
					offset = 0f;
					break;
			}
		}
		
		GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(offset, 0f));
	}
}
