﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossProjectile : MonoBehaviour
{
	public GameObject projectile;
	public float speed;
	public float damage;
	
	Vector3 pos;
	
	public void Start()
	{
		pos = new Vector3(GlobalBehavior.playerData.player.transform.position.x, 0f,
						  GlobalBehavior.playerData.player.transform.position.z);
	}
	
    void Update()
    {
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		transform.position = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);
		
        if (Mathf.Abs(transform.position.x - pos.x) <= speed * Time.deltaTime &&
			Mathf.Abs(transform.position.z - pos.z) <= speed * Time.deltaTime) {
				float initAngle = Random.Range(0f, 60.0f);
				Instantiate(projectile, pos, Quaternion.Euler(90f, initAngle, 90f));
				Instantiate(projectile, pos, Quaternion.Euler(90f, initAngle + 60f, 90f));
				Instantiate(projectile, pos, Quaternion.Euler(90f, initAngle + 120f, 90f));
				Instantiate(projectile, pos, Quaternion.Euler(90f, initAngle + 180f, 90f));
				Instantiate(projectile, pos, Quaternion.Euler(90f, initAngle + 240f, 90f));
				Instantiate(projectile, pos, Quaternion.Euler(90f, initAngle + 300f, 90f));
				Destroy(gameObject);
		}
    }
	
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "playership") {
			PlayerCombat script = col.gameObject.GetComponent<PlayerCombat>();
			float remainingDamage = damage * GlobalBehavior.difficultyModifier - script.curShield;
			script.curShield = Mathf.Clamp(script.curShield - damage * GlobalBehavior.difficultyModifier, 0f, 50f);
			if (remainingDamage > 0f)
				script.curHealth -= remainingDamage;
			Destroy(gameObject);
		}
	}
}
