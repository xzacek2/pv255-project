﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
	public GameObject laserPrefab;
	GameObject tmp;
	GameObject laser;
	float timer;
	float side;
	
    public void Update()
	{
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		if (laser != null) {
			laser.transform.position = tmp.transform.position;
			timer -= Time.deltaTime;
			laser.GetComponent<LineRenderer>().SetPosition(0, tmp.transform.position);
			laser.transform.Rotate(0f, 30f * Time.deltaTime * side, 0f);
			if (timer < 0f) {
				Destroy(laser);
				laser = null;
			}
		}
		
		transform.Find("LaserRight").Rotate(0f, 0f, -30f * Time.deltaTime);
		transform.Find("LaserLeft").Rotate(0f, 0f, 30f * Time.deltaTime);
	}
	
    public void leftLaser()
	{
		tmp = gameObject.transform.Find("LaserLeft").gameObject;
		laser = Instantiate(laserPrefab, new Vector3(0f, 0f, 0f), Quaternion.Euler(0f, 0f, 0f));
		laser.transform.position = tmp.transform.position;
		laser.GetComponent<LineRenderer>().SetPosition(0, tmp.transform.position);
		timer = 4f;
		side = -1f;
	}
	
	public void rightLaser()
	{
		tmp = gameObject.transform.Find("LaserRight").gameObject;
		laser = Instantiate(laserPrefab, new Vector3(0f, 0f, 0f), Quaternion.Euler(0f, 180f, 0f));
		laser.transform.position = tmp.transform.position;
		laser.GetComponent<LineRenderer>().SetPosition(0, tmp.transform.position);
		timer = 4f;
		side = 1f;
	}
}
