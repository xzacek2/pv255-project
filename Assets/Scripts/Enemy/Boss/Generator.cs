﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Generator : MonoBehaviour
{
	public float curHealth;
	public float maxHealth;
	public bool isImmune = false;
	public Camera cam;
	
	public GameObject canvas;
	public GameObject hp;
	
    void Start()
    {
		curHealth = maxHealth;
        hp.GetComponent<Slider>().value = curHealth / maxHealth;
		cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
    }

    void LateUpdate()
    {
        hp.GetComponent<Slider>().value = curHealth / maxHealth;
		canvas.transform.position = transform.position + new Vector3(-2f, 0f, 0f);
		canvas.transform.LookAt(canvas.transform.position + cam.transform.forward);
		canvas.transform.Rotate(0f, 0f, 0f);
    }
}
