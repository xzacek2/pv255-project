﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossMovement : StateMachineBehaviour
{
	GameObject boss;
	Rigidbody bossRB;
	float timer;
	
	public float speed;
	
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		boss = GameObject.FindWithTag("Boss");
		bossRB = boss.GetComponent<Rigidbody>();
		animator.transform.Find("Center").Find("Base").GetComponent<FirePlasma>().enabled = true;
		animator.transform.Find("Center").Find("FireEffect").gameObject.SetActive(true);
		timer = 3f;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		Vector3 target = new Vector3(0f, 0f, GlobalBehavior.playerData.player.transform.position.z);
		animator.transform.position = Vector3.MoveTowards(boss.transform.position, target, speed * Time.deltaTime);
		
		if (timer > 0f)
			timer -= Time.deltaTime;
			
		if (GlobalBehavior.playerData.player.transform.position.z > 5f && timer <= 0f &&
			animator.transform.Find("Center").Find("LaserLeft").gameObject.activeSelf) {
				animator.SetTrigger("LeftLaser");
				animator.transform.Find("Center").Find("LaserLeft").GetComponent<Generator>().isImmune = true;
		} else if (GlobalBehavior.playerData.player.transform.position.z < -5f && timer <= 0f &&
				animator.transform.Find("Center").Find("LaserRight").gameObject.activeSelf) {
					animator.SetTrigger("RightLaser");
					animator.transform.Find("Center").Find("LaserRight").GetComponent<Generator>().isImmune = true;
		}
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.ResetTrigger("LeftLaser");
        animator.ResetTrigger("RightLaser");
    }
}
