﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirePlasma : MonoBehaviour
{
	public GameObject projectile;
	float fireCooldown;
	
    void Update()
    {
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		if (fireCooldown > 0)
			fireCooldown -= Time.deltaTime;
		else {
			fireCooldown = 1.5f;
			Instantiate(projectile, transform.position - new Vector3(4f, transform.position.y, 0f), Quaternion.Euler(90f, 0f, 90f));
		}
    }
}
