﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserAttack : StateMachineBehaviour
{
	GameObject boss;
	Rigidbody bossRB;
	GameObject player;
	
	float side = 0f;
	float sidePoint;
	bool goBack;
	
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		boss = GameObject.FindWithTag("Boss");
		bossRB = boss.GetComponent<Rigidbody>();
		player = GlobalBehavior.playerData.player;
		animator.transform.Find("Center").Find("Base").GetComponent<FirePlasma>().enabled = false;
		animator.transform.Find("Center").Find("FireEffect").gameObject.SetActive(false);
		sidePoint = Camera.main.ScreenToWorldPoint(new Vector3(0f, Screen.height / 2f, 20f)).z - 10f;
		goBack = false;
		
		if (stateInfo.IsName("LeftLaserAttack"))
			side = 1f;
		else if (stateInfo.IsName("RightLaserAttack"))
			side = -1f;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		if (GlobalBehavior.playerData.player.GetComponent<PlayerCombat>().curHealth <= 0 || GlobalBehavior.bossDefeated)
			return;
		
		if (goBack) {
			if (Mathf.Abs(bossRB.position.z - (-side * sidePoint)) < 0.001f)
				animator.SetTrigger("Movement");
			else
				animator.transform.position = Vector3.MoveTowards(boss.transform.position, new Vector3(0f, 0f, -side * sidePoint), (Mathf.Abs(sidePoint) / 2f) * Time.deltaTime);
		} else {
			if (Mathf.Abs(bossRB.position.z - (side * sidePoint)) < 0.001f) {
				goBack = true;
				if (side == 1f)
					boss.transform.Find("Center").gameObject.GetComponent<LaserScript>().leftLaser();
				else
					boss.transform.Find("Center").gameObject.GetComponent<LaserScript>().rightLaser();
			} else
				animator.transform.position = Vector3.MoveTowards(boss.transform.position, new Vector3(0f, 0f, side * sidePoint), 6f * Time.deltaTime);
		}
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
		if (side == 1f)
			animator.transform.Find("Center").Find("LaserLeft").GetComponent<Generator>().isImmune = false;
		else if (side == -1f)
			animator.transform.Find("Center").Find("LaserRight").GetComponent<Generator>().isImmune = false;
		
        animator.ResetTrigger("Movement");
    }
}
