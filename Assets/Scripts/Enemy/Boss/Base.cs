﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Base : MonoBehaviour
{
	public float curHealth = 500f;
	public float maxHealth = 500f;
	public float dmgMultiplier = 0f;
	
	public GameObject hp;
	
	void Start()
    {
		curHealth = maxHealth;
		hp = GameObject.FindWithTag("bosshp");
        hp.GetComponent<Slider>().value = curHealth / maxHealth;
    }

    void LateUpdate()
    {
        hp.GetComponent<Slider>().value = curHealth / maxHealth;
    }
}
